/*
 *  Digilent HYGRO on an OLED Temperature and Humidity viewer
 *  
 *  Digilent code copyright Digilent
 *  Small pieces from the Adafruit OLED SSD1306 code
 *  The rest Copyright Crandall, use as you will
 *  
 *  Contributors:
 *    Aaron S. Crandall <acrandal@gmail.com> - 2019
 */


#include <SPI.h>
#include <Wire.h>

#include "ClosedCube_HDC1080.h"

#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define SCREEN_WIDTH 128      // OLED display width, in pixels
#define SCREEN_HEIGHT 64      // OLED display height, in pixels
#define OLED_RESET     4      // Reset pin # (or -1 if sharing Arduino reset pin)

// Declaration for an SSD1306 display connected to I2C (SDA, SCL pins)
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);
 
ClosedCube_HDC1080 hdc1080;       // Create a TI HDC1080 object


// *************************************************************************** //
void setup() {
  // put your setup code here, to run once:

  Serial.begin(115200);
  delay(1000);                // Allow serial connection to fully initialize
  Serial.println("ClosedCube HDC1080 Arduino Test");

  hdc1080.begin(0x40);        // Connect to HDC1080's I2C bus

  // Dump TI HDC1080 device information
  Serial.print("Manufacturer ID=0x");
  Serial.println(hdc1080.readManufacturerId(), HEX); // 0x5449 ID of Texas Instruments
  Serial.print("Device ID=0x");
  Serial.println(hdc1080.readDeviceId(), HEX); // 0x1050 ID of the device
 
  printSerialNumber();                        // Prints the IT HDC1080 device's serial num

  // Intialize the OLED screen
  // NOTE: The OLED that I had on hand is a cheap knockoff
  //  The driver is the same, but for some INSANE reason they use 0x3C as the I2C address
  //  Adafruit uses 0x3D for the same device
  if(!display.begin(SSD1306_SWITCHCAPVCC, 0x3C)) { // Address 0x3D for 128x64
    Serial.println(F("SSD1306 allocation failed"));
    for(;;); // Don't proceed, loop forever
  }

  display.clearDisplay();
  display.display();
}


// *************************************************************************** //
void loop() {
  Serial.print("T=");
  Serial.print(hdc1080.readTemperature());
  Serial.print("C, RH=");
  Serial.print(hdc1080.readHumidity());
  Serial.println("%");

  float temp = hdc1080.readTemperature(); // Grab current temperature
  float humid = hdc1080.readHumidity();   // Grab current humidity

  display.clearDisplay();
  display.setTextSize(2);               // Normal 1:1 pixel scale
  display.setTextColor(SSD1306_WHITE);  // Draw white text
  display.setCursor(0,0);               // Start at top-left corner
  display.println("BEST EVAR~");        // println does a carriage return + form feed
  display.print("T: ");
  display.print(temp);
  display.println("C");
  display.print("H: ");
  display.print(humid);
  display.println("%");
  display.display();

  delay(500);
}


// *************************************************************************** //
void printSerialNumber() {
  Serial.print("Device Serial Number=");
  HDC1080_SerialNumber sernum = hdc1080.readSerialNumber();
  char format[12];
  sprintf(format, "%02X-%04X-%04X", sernum.serialFirst, sernum.serialMid, sernum.serialLast);
  Serial.println(format);
}
