# Digilent HYGRO on an OLED Temperature and Humidity viewer

A small demo project I whipped up at the WSU Hardware Hackathon 2019. It uses an Arduino or Teensy, Digilent HYGRO PMod and a simple SSD1306 128x64 OLED screen to show the current sensor readings.

This was written between 12:01am and 1:00am so don't hold me to comments, formatting or code structure. It is mostly a melding of the demo code from Digilent and a few excerpts from Adafruit's OLED driver.


### Hardware components

Teensy 4.0 board used:
https://www.pjrc.com/store/teensy40.html

Digilent HYGRO humidity and temperature sensor used:
https://store.digilentinc.com/pmod-hygro-digital-humidity-and-temperature-sensor/

I2C 0.96" OLED 128x64 with a SSD1306 driver chipset:
https://www.elecrow.com/i2c-096-oled-128x64-blueyellow-p-1086.html


### License:
1. The Digilent parts are theirs
2. The Adafruit snippets would be theirs
3. Whatever else is public domain - Aaron S. Crandall
